FROM python:3.8
COPY . .
WORKDIR /code/
RUN pip install -r requirements.txt
CMD ["python", "bot.py"]
